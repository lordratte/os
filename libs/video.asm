;***
; Requires:
;   mov    si, <start of sting>
;***
print:
.loop:  lodsb
        cmp   al, 0x00
        je     .end
        mov    ah, 0x0e
        int    0x10
        jmp    .loop
.end ret

;***
; Requires:
;	mov		ah, <number>
;***

hex_to_char:
    mov     dl, ah
    xor     ax, ax
    mov     al, dl
    and     al, 0xF0
    shr     al, 4
    mov     bx, hex_table
    add     bx, ax
    mov     al, byte [bx]
    mov     byte [hex_val], al
    
    xor     ax, ax
    mov     al, dl
    and     al, 0x0F
    mov     bx, hex_table
    add     bx, ax
    mov     al, byte [bx]
    mov     byte [hex_val+1], al
    ret
hex_table: db "0123456789ABCDEF"
hex_val: db 0, 0, 0
