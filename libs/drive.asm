;***
; Requires:
;	mov		dl, <drive code>
; Output:
;   hex_val
;***
reset_drive:
    mov     ah, 0
  	int	    0x13

    mov	    ah, 1
  	int		  0x13
    call    hex_to_char
    mov     si, hex_val
    call    print
	ret

;***
; Requires:
;   mov		ax, <output loc>
;	mov		dl, <drive code>
;***
read_drive:
    mov		es, ax
	  xor		  bx, bx
    mov	  	ah, 0x02
    mov	  	al, 1
    mov 		ch, 1
    mov	  	cl, 2
    mov 		dh, 0
    int     0x13

    mov     ah, 1
    int     0x13
    call    hex_to_char
    mov     si, hex_val
    call    print
	ret
