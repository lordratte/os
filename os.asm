;*********************************************
;    os.asm
;        - A Simple Bootloader
;*********************************************

bits 16
org 0x7C00

start:
    ; Save dh (drive booted from) to load later on
    push    dx

    ; Print "Reset and Read Drive: \n"
    mov     si, msg_load_drives_boot
    call    print_boot
    mov     si, newline_boot
    call    print_boot

    ; Reset head of boot-drive
    call    reset_drive_boot

    ; Read second sector of boot-drive into memory just after boot-code.
    ; ax possibly needs to be 0x100 but that also doesn't work.
    pop     dx
    mov     ax, 0x200
    call    read_drive_boot

    mov     si, newline_boot
    call    print_boot

    ; Go to the start of the kernal. Must be the same as ax above e.g. 0x200 or 0x100.
    cli
    jmp     0x200

;***
; Requires:
;	mov		dl, <drive code>
; Output:
;   hex_val_boot
;***
reset_drive_boot:
    ; Attemt to reset
    mov     ah, 0
  	int	    0x13

    ; Display result code
    mov	    ah, 1
  	int     0x13
    call    hex_to_char_boot
    mov     si, hex_val_boot
    call    print_boot
	ret

;***
; Requires:
;   mov		ax, <output loc>
;	mov		dl, <drive code>
;***
read_drive_boot:
    ; Attempt to read
    mov		es, ax
    xor     bx, bx
    mov	  	ah, 0x02
    mov	  	al, 1
    mov     ch, 1
    mov	  	cl, 2 ; Sector
    mov 	dh, 0
    int     0x13

    mov     ah, 1
    int     0x13

    ; Display result code
    call    hex_to_char_boot
    mov     si, hex_val_boot
    call    print_boot
	ret

;***
; Requires:
;   mov    si, <start of sting>
;***
print_boot:
.loop:  lodsb
        cmp   al, 0x00
        je     .end
        mov    ah, 0x0e
        int    0x10
        jmp    .loop
.end ret

;***
; Requires:
;	mov		ah, <number>
;***

hex_to_char_boot:
    mov     dl, ah
    xor     ax, ax
    mov     al, dl
    and     al, 0xF0
    shr     al, 4
    mov     bx, hex_table_boot
    add     bx, ax
    mov     al, byte [bx]
    mov     byte [hex_val_boot], al

    xor     ax, ax
    mov     al, dl
    and     al, 0x0F
    mov     bx, hex_table_boot
    add     bx, ax
    mov     al, byte [bx]
    mov     byte [hex_val_boot+1], al
    ret

;************
; Data labels
;************
hex_table_boot: db "0123456789ABCDEF"
hex_val_boot: db 0, 0, 0

msg_load_drives_boot:    db     "Reset and Read Drive: ", 0
msg_halt_boot:           db      0xa, 0xd, "Halt", 0
newline_boot:            db      0xa,0xd,0

times 510 - ($-$$) db 0
dw 0xAA55
