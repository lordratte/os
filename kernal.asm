;*********************************************
;    kernal.asm
;        - A work-in-progress kernal
;*********************************************

bits 16
org 0x7E00

main:
    mov     ah, 0x1F
    call    hex_to_char
    mov     si, hex_val
    call    print
    hlt

  %include 'libs/drive.asm'
  %include 'libs/video.asm'
