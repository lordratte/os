==============================
#An Operating System... almost
===============================

So far, just a humble boot-loader, this program has crawled out of the primordial ooze of the digital world. What will it do? I hope I have the luxury of finding out.


###Current issue:
Making the second stage run.

##Build process
1. `os.asm` and `kernal.asm` are compiled into raw binary as the files `isofiles/boot.bin` and `isofiles/kernal.bin` respectively.
2. They are are then merged into one binary file `isofiles/main/main.bin` with `isofiles/boot.bin` coming first.
3. mkisofs is then used to turn the folder `isofiles/main/` into the file `os.iso`.
4. The `os.iso` can then be booted with a virtual machine like VirtualBox.
