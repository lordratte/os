build:
	rm -rf isofiles/*
	mkdir isofiles/main

	nasm -f bin -o isofiles/boot.bin os.asm
	nasm -f bin -o isofiles/kernal.bin kernal.asm

	cat	isofiles/boot.bin > isofiles/main/main.bin
	cat isofiles/kernal.bin >> isofiles/main/main.bin
	mkisofs -o os.iso -b main.bin -no-emul-boot -boot-load-size 1 -R -J -T isofiles/main/

clean:
	rm os.iso
	rm *~
	rm test/*
	rm -rf isofiles/*

start:
	VBoxManage startvm MyOS
